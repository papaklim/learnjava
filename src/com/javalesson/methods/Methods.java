package com.javalesson.methods;

public class Methods {
    public static void main(String[] args) {
        printMessage("Jopa");
        System.out.println("Rectangle square: " + calcRectangleSquare(5, 10));
        System.out.println("Square: " + calcSquare(10));
        System.out.println("Sum of squares: " + (calcRectangleSquare(5, 10) + calcSquare(10)));


        String str1 = "I Like Black Coffee";
        String str3 = "I Like Coffee!!!";

        System.out.println(str1.toUpperCase());
        System.out.println(str1.toLowerCase());
        String substring = str3.substring(0, 13);
        String str2 = "I Like Coffee";
        String str4 = new String("I Like Coffee");

        System.out.println("Substring: "+substring);
        boolean b = str2 == substring;
        boolean c = str2.equals(str4);
//        System.out.println(b);
//        System.out.println(c);

        boolean like = str1.startsWith(" Like",2);
        System.out.println(like);
        System.out.println(str1.replace("Black", "White"));

        System.out.println(str3.indexOf("!!!"));
    }

    static void printMessage(String name) {
        System.out.println("Hello, " + name + "!!!");
    }

    static int calcRectangleSquare(int x, int y) {
        int square = x * y;
//        System.out.println("Rectangle square: " + square);
        return square;

    }

    static int calcSquare(int x) {
        return x * x;
    }
}