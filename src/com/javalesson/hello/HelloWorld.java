package com.javalesson.hello;

public class HelloWorld {

    public static void main(String[] args) {
        // write your code here
        System.out.println("Hello World");

        byte b = 127;
        short c = 666;
        int d = 60000;
        int a = b + 128;
        long myLongValue = 7_777_789_456_345_122_678L;
        float floatValue = 123.12F;
        double doubleValue = 123.1245678;
        double sum = a + b + c + d + myLongValue + floatValue + doubleValue;
        char char1 = 'a';
        char char2 = 'B';
        char char3 = '^';
        char char4 = '\u30B6';
        char char5 = '\u20A9';
        System.out.println("char1: " + char1 + "\n" + "char2: " + char2 + "\n" +
                "char3: " + char3 + "\n" + "char4: " + char4 + "\n" + "char5: " + char5);
        System.out.println(char1 + char2 + char3 + char4 + char5);

        System.out.println("b: " + b);
        System.out.println("a: " + a);
        System.out.println("c: " + c);
        System.out.println("d: " + d);
        System.out.println("myLongValue: " + myLongValue);
        System.out.println("floatValue: " + floatValue);
        System.out.println("doubleValue: " + doubleValue);
        System.out.println("sum: " + sum);

        boolean b1 = true;
        boolean b2 = false;

        String s = "some string + " + "some another string";
        System.out.println(s);

        double longToDouble = myLongValue;
        System.out.println("longToDouble: "+longToDouble);
        long doubleToLong = (long) longToDouble;
        System.out.println("doubleToLong: "+doubleToLong);

    }
}
