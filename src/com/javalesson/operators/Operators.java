package com.javalesson.operators;

public class Operators {
    public static void main(String[] args) {
//        + - / * %
        double a = 10;
        double b = 3;

        double c = a + b;
        double d = a - b;

        double e = a * b;
        double f = a / b;

        double g = a % b;
        System.out.println("a: " + a);
        System.out.println("b: " + b);
        System.out.println("c: " + c);
        System.out.println("d: " + d);
        System.out.println("e: " + e);
        System.out.println("f: " + f);
        System.out.println("g: " + g);

//        a = a+1;
//        a += 1;
//        a++;
        a += 5;
        System.out.println("New \"a\": " + a);

        b--;
        System.out.println("New \"b\": " + b);

        double n = 7;
        double m = 7;

        double res1 = 2 * n++;
        double res2 = 2 * ++m;

        System.out.println("Result 1: " + res1);
        System.out.println("Result 2: " + res2);
        System.out.println("n: " + n);
        System.out.println("------------------------------");
//        == != < > <= >= && || ?:

        int x = 3;
        int y = 5;
        int z = 8;

        boolean boolVal1 = x == y;
        boolean boolVal2 = x <= y;
        boolean boolRes = boolVal1 || boolVal2;

        System.out.println(boolVal1);
        System.out.println(boolVal2);
        System.out.println(boolRes);

        int ternaryRes = (x > y) && (z < y) ? x : y;

        System.out.println(ternaryRes);

    }
}
