package com.javalesson.controlstatement;

public class SwitchCase {
    public static void main(String[] args) {

        String dayOfTheWeek = args[0];
        switch (dayOfTheWeek.toLowerCase()) {
            case "monday":
                System.out.println(dayOfTheWeek + " is the first working day");
                break;
            case "tuesday":
                System.out.println(dayOfTheWeek + " is the second working day");
                break;
            case "wednesday":
                System.out.println(dayOfTheWeek + " is the third working day");
                break;
            case "thursday":
                System.out.println(dayOfTheWeek + " is the forth working day");
                break;
            case "friday":
                System.out.println(dayOfTheWeek + " is the fifth working day");
                break;
            case "saturday":
            case "sunday":
                System.out.println(dayOfTheWeek + " is the weekend day");
                break;
            default:
                System.out.println("Not a day of the week");
        }
    }
}
